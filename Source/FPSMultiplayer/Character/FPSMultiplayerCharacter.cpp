// Copyright Epic Games, Inc. All Rights Reserved.

#include "FPSMultiplayerCharacter.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/GameplayStatics.h"
#include "Game/FPSMultiplayerGameInstance.h"
#include "Net/UnrealNetwork.h"

// DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

AFPSMultiplayerCharacter::AFPSMultiplayerCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);
	
	GetMesh()->SetOwnerNoSee(true);
	
	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;
	
	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	// Create a gun mesh component
	SkeletalMeshFakeWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh Fake"));
	SkeletalMeshFakeWeapon->SetOwnerNoSee(true);
	SkeletalMeshFakeWeapon->bCastDynamicShadow = false;
	SkeletalMeshFakeWeapon->CastShadow = false;
	SkeletalMeshFakeWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshFakeWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshFakeWeapon->SetupAttachment(GetMesh());
	
	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));

	if (InventoryComponent)
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AFPSMultiplayerCharacter::InitWeapon);
		
	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.
	
	//Network
	bReplicates = true;
}

void AFPSMultiplayerCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	const FAttachmentTransformRules AttachRules = FAttachmentTransformRules::KeepRelativeTransform;
	SkeletalMeshFakeWeapon->AttachToComponent(GetMesh(), AttachRules, "WeaponSocketRightHand");
	
	// InitWeapon(InitWeaponName);
	
}

void AFPSMultiplayerCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	
}

//////////////////////////////////////////////////////////////////////////
// Input

void AFPSMultiplayerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind run events
	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &AFPSMultiplayerCharacter::Run);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &AFPSMultiplayerCharacter::StopRun);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AFPSMultiplayerCharacter::InputAttackPressed);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AFPSMultiplayerCharacter::InputAttackReleased);

	//Switch Weapon
	PlayerInputComponent->BindAction("TrySwitchNextWeapon", IE_Pressed, this, &AFPSMultiplayerCharacter::TrySwitchNextWeapon);
	PlayerInputComponent->BindAction("TrySwitchPrevWeapon", IE_Pressed, this, &AFPSMultiplayerCharacter::TrySwitchNextWeapon);
	
	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AFPSMultiplayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFPSMultiplayerCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "TurnRate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AFPSMultiplayerCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AFPSMultiplayerCharacter::LookUpAtRate);
}

void AFPSMultiplayerCharacter::CharacterUpdate()
{
	float ResSpeed = 300.0f;
	switch (MovementState)
	{
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void AFPSMultiplayerCharacter::ChangeMovementState_OnServer_Implementation(EMovementState NewMovementState)
{
	ChangeMovementState_Multicast(NewMovementState);
}

void AFPSMultiplayerCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void AFPSMultiplayerCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void AFPSMultiplayerCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->SetWeaponStateFire_OnServer(bIsFiring);
		if (!bIsFiring)
			CanRun = true;
		else
		{
			CanRun = false;
			StopRun();
		}
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("AFPSMultiplayerCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void AFPSMultiplayerCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AFPSMultiplayerCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AFPSMultiplayerCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AFPSMultiplayerCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
	if(IsLocallyControlled())
		SyncControlRotation_OnServer(GetControlRotation());
	
}

void AFPSMultiplayerCharacter::ChangeMovementState_Multicast_Implementation(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();
}

AWeaponDefault* AFPSMultiplayerCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void AFPSMultiplayerCharacter::InitWeapon(FName IdWeaponName, FAddicionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentWeaponIndex)
{
	UFPSMultiplayerGameInstance* myGI = Cast<UFPSMultiplayerGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);
            
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();
            
				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass,
																	&SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeaponInfo.SkeletalMeshGun)
					SetFakeWeapon_Multicast(myWeaponInfo.SkeletalMeshGun);
				
				if (Mesh1P && myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(Mesh1P, Rule, FName("GripPoint"));
					myWeapon->ShootLocation = GetFirstPersonCameraComponent();
					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->ProjectileSetting = myWeaponInfo.ProjectileSetting;
					myWeapon->OnWeaponFire.AddDynamic(this, &AFPSMultiplayerCharacter::FireAnimation);
					CurrentWeapon = myWeapon;
					CurrentWeaponIndex = NewCurrentWeaponIndex;
				}
			}	
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("AFPSMultiplayerCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

void AFPSMultiplayerCharacter::FireAnimation(UAnimMontage* FireAnim1P, UAnimMontage* FireAnim3P)
{
	// try and play a firing animation if specified
    	
	if (FireAnim1P != nullptr)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != nullptr)
		{
			AnimInstance->Montage_Play(FireAnim1P, 1.f);
		}
	}

	if (FireAnim3P != nullptr)
	FireStartAnim_OnServer(FireAnim3P);
}

void AFPSMultiplayerCharacter::Run()
{
	if (CanRun != false)
	ChangeMovementState_OnServer(EMovementState::Run_State);
}

void AFPSMultiplayerCharacter::StopRun()
{
	ChangeMovementState_OnServer(EMovementState::Walk_State);
}

void AFPSMultiplayerCharacter::TrySwitchNextWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more the one weapon fo switch
		int8 OldIndex = CurrentWeaponIndex;
		FAddicionalWeaponInfo OldInfo;
		
		// if (CurrentWeapon)
		// {
		// 	OldInfo = CurrentWeapon->AdditionalWeaponInfo;
		// 	if (CurrentWeapon->WeaponReloading)
		// 		CurrentWeapon->CancelReload();
		// }

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentWeaponIndex + 1, OldIndex, OldInfo, true))
			{ }
		}
	}
	
}

void AFPSMultiplayerCharacter::TrySwitchPrevWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more the one weapon fo switch
		int8 OldIndex = CurrentWeaponIndex;
		FAddicionalWeaponInfo OldInfo;
		// if (CurrentWeapon)
		// {
		// 	OldInfo = CurrentWeapon->AdditionalWeaponInfo;
		// 	if (CurrentWeapon->WeaponReloading)
		// 		CurrentWeapon->CancelReload();
		// }

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentWeaponIndex - 1, OldIndex, OldInfo, false))
			{}
		}
	}
}

void AFPSMultiplayerCharacter::SetFakeWeapon_Multicast_Implementation(USkeletalMesh* FakeGunMesh)
{
	SkeletalMeshFakeWeapon->SetSkeletalMesh(FakeGunMesh);
}

void AFPSMultiplayerCharacter::SyncControlRotation_OnServer_Implementation(FRotator CurrentRotation)
{
	if (!IsLocallyControlled())
		FirstPersonCameraComponent->SetWorldRotation(CurrentRotation);
}

void AFPSMultiplayerCharacter::FireStartAnim_OnServer_Implementation(UAnimMontage* Anim)
{
	FireStartAnim_Multicast(Anim);
}

void AFPSMultiplayerCharacter::FireStartAnim_Multicast_Implementation(UAnimMontage* Anim)
{
	// Get the animation object for the body mesh
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance != nullptr)
	{
		AnimInstance->Montage_Play(Anim, 1.f);
	}
}

 void AFPSMultiplayerCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
 {
 	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

 	DOREPLIFETIME(AFPSMultiplayerCharacter, MovementState);
	DOREPLIFETIME(AFPSMultiplayerCharacter, CurrentWeapon);
	DOREPLIFETIME(AFPSMultiplayerCharacter, CurrentWeaponIndex);
 }