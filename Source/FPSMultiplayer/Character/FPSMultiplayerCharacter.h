// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Weapon/WeaponDefault.h"
#include "FuncLibrary/Types.h"
#include "InventoryComponent.h"
#include "FPSMultiplayerCharacter.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class USceneComponent;
class UCameraComponent;
class UAnimMontage;
class USoundBase;

UCLASS(config=Game)
class AFPSMultiplayerCharacter : public ACharacter
{
	GENERATED_BODY()
	
	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	USkeletalMeshComponent* Mesh1P;
	
	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* SkeletalMeshFakeWeapon;


	
public:
	AFPSMultiplayerCharacter();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UInventoryComponent* InventoryComponent;

protected:
	virtual void BeginPlay() override;
	
	virtual void Tick(float DeltaSeconds) override;

public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

protected:

	// APawn interface
    virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
    // End of APawn interface
	
	/** Fires a projectile. */
	void InputAttackPressed();
	void InputAttackReleased();

	virtual void GetLifetimeReplicatedProps(TArray < class FLifetimeProperty > & OutLifetimeProps) const override;
	
	void AttackCharEvent(bool bIsFiring);
	
	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/* 
	 * Configures input for touchscreen devices if there is a valid touch interface for doing so 
	 *
	 * @param	InputComponent	The input component pointer to bind controls to
	 * @returns true if touch controls were enabled.
	 */

public:
	
	/** Returns Mesh1P subobject **/
	USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent;}
	
	UPROPERTY(Replicated);
	EMovementState MovementState = EMovementState::Walk_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement")
	FCharacterSpeed MovementInfo;
	
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	UFUNCTION(Server, Reliable)
	void ChangeMovementState_OnServer(EMovementState NewMovementState);
	UFUNCTION(NetMulticast, Reliable)
	void ChangeMovementState_Multicast(EMovementState NewMovementState);

	bool CanRun = true;
	
	//Weapon

	UPROPERTY(Replicated)
	AWeaponDefault* CurrentWeapon = nullptr;
	UPROPERTY(Replicated)
	int32 CurrentWeaponIndex = 0;
	
	//for demo 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	FName InitWeaponName;
	
	UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IdWeaponName, FAddicionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentWeaponIndex);

	UFUNCTION()
	void FireAnimation(UAnimMontage* FireAnima1P, UAnimMontage* FireAnim3P);
	
	void Run();
	void StopRun();

	void TrySwitchNextWeapon();
	void TrySwitchPrevWeapon();
	
	//Net
	UFUNCTION(Server, Unreliable)
	void FireStartAnim_OnServer(UAnimMontage* Anim);
	UFUNCTION(NetMulticast, Unreliable)
	void FireStartAnim_Multicast(UAnimMontage* Anim);
	UFUNCTION(Server, Reliable)
	void SyncControlRotation_OnServer(FRotator CurrentRotation);
	UFUNCTION(NetMulticast, Unreliable)
	void SetFakeWeapon_Multicast(USkeletalMesh* FakeGunMesh);
};

