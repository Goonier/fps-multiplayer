// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "FuncLibrary/Types.h"
#include "FPSMultiplayerPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class FPSMULTIPLAYER_API AFPSMultiplayerPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Weapon")
	TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Weapon")
	TArray<FAmmoSlot> AmmoSlots;
	
};
