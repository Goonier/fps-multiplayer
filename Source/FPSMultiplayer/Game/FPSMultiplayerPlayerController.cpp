// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/FPSMultiplayerPlayerController.h"

#include "FPSMultiplayerPlayerState.h"
#include "Character/FPSMultiplayerCharacter.h"

void AFPSMultiplayerPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	AFPSMultiplayerCharacter* myCharacter = Cast<AFPSMultiplayerCharacter>(InPawn);
	AFPSMultiplayerPlayerState* myState = GetPlayerState<AFPSMultiplayerPlayerState>();
	
	if (myCharacter && myState)
	{
		myCharacter->InventoryComponent->InitInventory_OnServer(myState->WeaponSlots, myState->AmmoSlots);
	}
}
