// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "FPSMultiplayerHUD.generated.h"

UCLASS()
class AFPSMultiplayerHUD : public AHUD
{
	GENERATED_BODY()

public:
	AFPSMultiplayerHUD();
	/** Primary draw call for the HUD */
	virtual void DrawHUD();

	//UFUNCTION(BlueprintNativeEvent, Category = "Sight")
	void DrawSight(UTexture2D* Sight);
	//virtual void DrawSight_Implementation(UTexture2D* Sight);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sight")
	UTexture2D* SightTexture = nullptr;

private:


};

