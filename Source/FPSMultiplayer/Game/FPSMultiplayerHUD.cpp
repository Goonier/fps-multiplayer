// Copyright Epic Games, Inc. All Rights Reserved.

#include "FPSMultiplayerHUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"

AFPSMultiplayerHUD::AFPSMultiplayerHUD()
{

}

void AFPSMultiplayerHUD::DrawHUD()
{
	Super::DrawHUD();

	DrawSight(SightTexture);

}

void AFPSMultiplayerHUD::DrawSight(UTexture2D* Sight)
{
	if (Sight)
	{
		// Draw very simple crosshair

		// find center of the Canvas
		const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

		// offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
		const FVector2D CrosshairDrawPosition((Center.X),
			(Center.Y));

		// draw the crosshair
		FCanvasTileItem TileItem(CrosshairDrawPosition, Sight->Resource, FLinearColor::White);
		TileItem.BlendMode = SE_BLEND_Translucent;
		Canvas->DrawItem(TileItem);
	}
}
