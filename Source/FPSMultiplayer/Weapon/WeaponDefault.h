// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "FuncLibrary/Types.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponFire, UAnimMontage*, FireAnim1P, UAnimMontage*, FireAnim3P);

UCLASS()
class FPSMULTIPLAYER_API AWeaponDefault : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleDefaultsOnly, Category=Scene)
	USceneComponent* Scene;
	
	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* SkeletalMeshWeapon;

	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	UStaticMeshComponent* StaticMeshWeapon;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	UArrowComponent* ShootFXLocationArrow;
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponFire OnWeaponFire;
	
	UPROPERTY()
	FWeaponInfo WeaponSetting;
	UPROPERTY()
	FProjectileInfo ProjectileSetting;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray < class FLifetimeProperty > & OutLifetimeProps) const override;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* ShootLocation = nullptr;

	//UPROPERTY(Replicated)
	//FRotator ControlRotationSynchronized;

	UPROPERTY(BlueprintReadWrite)
	bool bIsLocalController;
	
	void FireTick(float DeltaTime);

	void WeaponInit();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponFiring = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponReloading = false;
	
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetWeaponStateFire_OnServer(bool bIsFire);

	UFUNCTION(NetMulticast, Unreliable)
	void WeaponSoundFX_Multicast(const UObject* WorldContextObject, USoundBase* ShootSound, FVector Location);
	
	bool CheckWeaponCanFire();
	
	/** Fires a projectile. */
	void Fire();

	//Timers'flags
	float FireTime = 0.0;
	
};
