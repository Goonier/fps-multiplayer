// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"

#include "DrawDebugHelpers.h"
#include "ProjectileDefault.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

int32 DebugWeaponShow = 1;
FAutoConsoleVariableRef CVarWeaponShow(
	TEXT("FPS.DebugWeapon"),
	DebugWeaponShow,
	TEXT("Draw Debug for Weapon"),
	ECVF_Cheat);

// Sets default values
AWeaponDefault::AWeaponDefault()
{
	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	SetRootComponent(Scene);
	
	// Create a gun mesh component
	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetOnlyOwnerSee(true);			// otherwise won't be visible in the multiplayer
	SkeletalMeshWeapon->bCastDynamicShadow = false;
	SkeletalMeshWeapon->CastShadow = false;
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetOnlyOwnerSee(true);			// otherwise won't be visible in the multiplayer
	StaticMeshWeapon->bCastDynamicShadow = false;
	StaticMeshWeapon->CastShadow = false;
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);
	
	ShootFXLocationArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootFXLocationArrow->SetupAttachment(SkeletalMeshWeapon);
	ShootFXLocationArrow->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));
	
	PrimaryActorTick.bCanEverTick = true;

	//Network
	bReplicates = true;
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasAuthority())
	FireTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (WeaponFiring)
		if (FireTime < 0.f)
			Fire();
		else
			FireTime -= DeltaTime;
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
}

void AWeaponDefault::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
}

void AWeaponDefault::WeaponSoundFX_Multicast_Implementation(const UObject* WorldContextObject, USoundBase* ShootSound, FVector Location)
{
	UGameplayStatics::PlaySoundAtLocation(WorldContextObject, ShootSound, Location);
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return true;
}

void AWeaponDefault::Fire()
{
	FireTime = WeaponSetting.RateOfFire;
	
	// try and fire a projectile
	if (ShootLocation != nullptr)
	{
		FTransform Offset = WeaponSetting.ShootLocationOffset;
		FVector LocalDir = ShootLocation->GetForwardVector() * Offset.GetLocation().X + ShootLocation->GetRightVector() * Offset.GetLocation().Y + ShootLocation->GetUpVector() * Offset.GetLocation().Z;
		
		FVector SpawnLocation = ShootLocation->GetComponentLocation() + LocalDir;

		FRotator SpawnRotation = SpawnRotation = ShootLocation->GetComponentRotation() + Offset.Rotator();
		
		if (ProjectileSetting.Projectile != nullptr)
		{
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
			ActorSpawnParams.Owner = GetOwner();
			ActorSpawnParams.Instigator = GetInstigator();
			
			AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileSetting.Projectile, &SpawnLocation, &SpawnRotation, ActorSpawnParams)); 
		}
		else
		{
			FHitResult Hit;
			TArray<AActor*> Actors;
			FVector EndLocation = ShootLocation->GetForwardVector()*WeaponSetting.DistanceTrace;
				
			EDrawDebugTrace::Type DebugTrace;
			if (DebugWeaponShow)
			{
				DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector() * WeaponSetting.DistanceTrace, FColor::Black, false, 5.f, (uint8)'\000', 0.f);
				DebugTrace = EDrawDebugTrace::ForDuration;
			}
			else
				DebugTrace = EDrawDebugTrace::None;
				
			UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSetting.DistanceTrace,
				ETraceTypeQuery::TraceTypeQuery1, true, Actors, DebugTrace, Hit, true, FLinearColor::Red, FLinearColor::Green, 3.0);
				
			if ((Hit.GetComponent() != nullptr) && Hit.GetComponent()->IsSimulatingPhysics())
				Hit.GetComponent()->SetPhysicsLinearVelocity(ShootLocation->GetForwardVector()*400.0f, true, NAME_None);
		}
		// try and play the sound if specified
		if (WeaponSetting.SoundFireWeapon != nullptr)
			WeaponSoundFX_Multicast(this, WeaponSetting.SoundFireWeapon, GetActorLocation());
	
		OnWeaponFire.Broadcast(WeaponSetting.AnimWeaponInfo.AnimCharFire1P, WeaponSetting.AnimWeaponInfo.AnimCharFire3P);
	}
}

// void AWeaponDefault::SyncControlRotation_OnServer_Implementation(FRotator CurrentRotation)
// {
// 	ControlRotationSynchronized = CurrentRotation;
// }

void AWeaponDefault::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//DOREPLIFETIME(AWeaponDefault, ControlRotationSynchronized);
 }
